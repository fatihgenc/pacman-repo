#!/bin/bash

# Set the paths for the working and output directories
WORKING_DIR="/data/pacman-repo/build" # Temporary working directory
OUTPUT_DIR="/data/pacman-repo/packages/x86_64"   # Directory to store built packages
BASE_DIR="/data/pacman-repo"
PACKAGES_DIR="/data/pacman-repo/packages"
VERSIONS_FILE="/data/pacman-repo/current_versions"
REPOS=(
    "https://aur.archlinux.org/httpdirfs.git"
    "https://aur.archlinux.org/onedriver.git"
    "https://aur.archlinux.org/youtube-music-bin.git"
    "https://aur.archlinux.org/rar.git"
    "https://aur.archlinux.org/ventoy-bin.git"
    "https://aur.archlinux.org/google-chrome.git"
    "https://aur.archlinux.org/freetube-bin.git"
    "https://aur.archlinux.org/buttercup-desktop.git"
    "https://aur.archlinux.org/cpu-x.git"
    "https://aur.archlinux.org/jdk.git"
    "https://aur.archlinux.org/soapui.git"
    "https://aur.archlinux.org/teams-for-linux.git"
    "https://aur.archlinux.org/visual-studio-code-bin.git"
    "https://aur.archlinux.org/zoom.git"
    "https://aur.archlinux.org/virtualbox-ext-oracle.git"
    "https://aur.archlinux.org/wps-office.git"
    "https://aur.archlinux.org/postman-bin.git"
    "https://aur.archlinux.org/microsoft-edge-stable-bin.git"
    "https://aur.archlinux.org/opera.git"
    "https://aur.archlinux.org/ttf-wps-fonts.git"
    "https://aur.archlinux.org/ttf-ms-fonts.git"
    "https://aur.archlinux.org/wps-office-fonts.git"
    "https://aur.archlinux.org/libtiff5.git"
    "https://aur.archlinux.org/ttf-ms-win11-auto.git"
    "https://aur.archlinux.org/youtube-music-bin.git"
    "https://aur.archlinux.org/webex-bin.git"
    "https://aur.archlinux.org/etcher-bin.git"
)

# Create directories if they don't exist
mkdir -p "$WORKING_DIR"
mkdir -p "$OUTPUT_DIR"
touch "$VERSIONS_FILE"

log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1"
}

get_pkg_info() {
    local key="$1"
    grep "^$key=" PKGBUILD | cut -d '=' -f 2 | tr -d '"'
}

log "Starting package build process..."

# Loop through repositories
for REPO in "${REPOS[@]}"; do
    REPO_NAME=$(basename "$REPO" .git)
    REPO_DIR="$WORKING_DIR/$REPO_NAME"
    cd "$WORKING_DIR" || { log "Failed to enter $WORKING_DIR"; continue; }

    log "Cloning repository: $REPO"
    git clone "$REPO" "$REPO_DIR" || { log "Failed to clone $REPO"; continue; }

    # Change to the repository directory
    cd "$REPO_DIR" || { log "Failed to enter $REPO_DIR"; continue; }

    # Extract pkgname and pkgver from PKGBUILD
    PKGNAME=$(get_pkg_info "pkgname")
    PKGVER=$(get_pkg_info "pkgver")
    PKGBASE=$(get_pkg_info "pkgbase")

    # If pkgname includes "-bin", use _pkgname instead
    if [[ "$PKGNAME" == *"-bin" ]]; then
        PKGNAME=$(get_pkg_info "_pkgname")
        if [[ -z "$PKGNAME" ]]; then
            log "Could not retrieve _pkgname for $REPO_NAME. Skipping..."
            rm -rf "$REPO_DIR"
            continue
        fi
    fi
   
    if [[ -n "$PKGBASE" ]]; then
        PKGNAME="$PKGBASE"
    fi	

    if [[ -z "$PKGNAME" || -z "$PKGVER" ]]; then
        log "Could not retrieve pkgname or pkgver for $REPO_NAME. Skipping..."
        rm -rf "$REPO_DIR"
        continue
    fi

    log "Package Info: Name=$PKGNAME, Version=$PKGVER"

    # Check version file for existing entry
    EXISTING_VER=$(grep "^$PKGNAME " "$VERSIONS_FILE" | cut -d ' ' -f 2)

    if [[ "$EXISTING_VER" == "$PKGVER" ]]; then
        log "Package $PKGNAME is already up-to-date. Skipping build..."
        rm -rf "$REPO_DIR"
        continue
    fi

    # Build the package
    log "Building package for $REPO_NAME..."
    makepkg -s --noconfirm || { log "Failed to build package for $REPO_NAME"; continue; }


    # Move built packages to the output directory
    log "Moving built package to output directory..."
    mv *.pkg.tar.zst* "$OUTPUT_DIR" || log "No package files found for $REPO_NAME"

    # Update version file
    log "Updating version file for $PKGNAME..."
    sed -i "/^$PKGNAME /d" "$VERSIONS_FILE" # Remove existing entry if any
    echo "$PKGNAME $PKGVER" >> "$VERSIONS_FILE"

    # Clean up the working directory for this repository
    log "Cleaning up working directory for $REPO_NAME..."
    rm -rf "$REPO_DIR"

done
 
    cd "$PACKAGES_DIR" || { log "Failed to enter $PACKAGES_DIR"; continue; }
    repo-add ifrepo.db.tar.gz x86_64/*.zst  
    touch "$VERSIONS_FILE"
    cd "$BASE_DIR" || { log "Failed to enter $BASE_DIR"; continue; }

    rsync -avz --progress packages/ deploy@ikilifikir.com.tr:/home/deploy/docker/pacman-repo.ikilifikir.com.tr/

log "Package build process completed."

